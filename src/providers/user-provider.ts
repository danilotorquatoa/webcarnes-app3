import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

import { User } from '../models/user';
import { JwtHelper } from 'angular2-jwt';

/*
Generated class for the UserProvider provider.

See https://angular.io/docs/ts/latest/guide/dependency-injection.html
for more info on providers and Angular 2 DI.
*/
@Injectable()
export class UserProvider {
  HAS_LOGGED_IN = 'hasLoggedIn';
  jwtHelper: JwtHelper = new JwtHelper();
  user: User;
  apiUrl = 'http://api.webcarnes.idx.digital/api/v1';

  constructor(
    public http: Http,
    public events: Events,
    private storage: Storage) {}

    login(email:string, password:string): Observable<User> {
      return this.http.post(
        this.apiUrl+'/login',
        {
          email : email,
          password : password
        }
      )
      .map( (res:Response) => new User ( res.json().data ) )
      .catch( (error:any) => Observable.throw( error.json().message || 'Server error') );
    }

    register(user:User): Observable<User> {

      if(!user['facebook_id']){
        delete user['facebook_id'];
      }

      let header = new Headers();
      header.append('Content-Type', 'application/json');
      return this.http.post(
        this.apiUrl+'/register',
        user,
        { headers: header }
      )
      .map( (res:Response) => new User ( res.json().data ) )
      .catch( (error:any) => Observable.throw( error.json() || 'Server error') );
    }

    recover(email:string){
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      return this.http.post(
        this.apiUrl+'/recover',
        email,
        { headers: header }
      )
      .map( (res:Response) => res.json().message )
      .catch( (error:any) => Observable.throw( error.json().message || 'Server error' ) );
    }

    reset(data:any){
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      return this.http.put(
        this.apiUrl+'/reset',
        data,
        { headers: header }
      )
      .map( (res:Response) => res.json().message )
      .catch( (error:any) => Observable.throw( error.json().message || 'Server error' ) );
    }

    refreshToken(){
      if(this.user.token){
        let header = new Headers();
        header.append('Authorization', 'Bearer '+ this.user.token);
        header.append('Content-Type', 'application/json');

        return this.http.get(
          this.apiUrl+'/refresh',
          { headers: header }
        )
        .map( (res:Response) => res.json().token )
        .catch( (error:any) => Observable.throw( error.json().message || 'Server error' ) );
      }
    }

    activate(code:string){
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      return this.http.post(
        this.apiUrl+'/activate',
        code,
        { headers: header }
      )
      .map( (res:Response) => res.json().message )
      .catch( (error:any) => Observable.throw( error.json().message || 'Server error' ) );
    }

    loginFBAccount(accessToken:string): Observable<User>{
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      return this.http.get(
        this.apiUrl+'/facebook/login?code='+accessToken,
        { headers: header }
      )
      .map( (res:Response) => new User ( res.json().data ) )
      .catch( (error:any) => Observable.throw( error.json().message || 'Server error' ) );
    }

    hasLoggedIn() {
      return this.storage.get('profile').then(
        (profile) => {
          if(profile && !this.jwtHelper.isTokenExpired(profile.token)){
            return true;
          }else if(profile && this.jwtHelper.isTokenExpired(profile.token)){
            this.refreshToken().subscribe(
              (token) => {
                profile.token = token;
                this.storage.set('profile',profile);
                return true;
              },(err)=>{
                console.log(JSON.stringify(err));
              }
            );
          }
          return false;
        }
      );
    }
  }
