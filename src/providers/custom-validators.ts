import { Component, Inject } from '@angular/core';
/*
  Generated class for the CustomValidators provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export class CustomValidators {

  static email(control) {
        var emailPattern = /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
        if (emailPattern.test(control.value)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

}
