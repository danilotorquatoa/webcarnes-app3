import { Facebook } from '@ionic-native/facebook';
import { Platform } from 'ionic-angular';
import { Injectable } from '@angular/core';
//import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the FbProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class FbProvider {
    private premissions: string[] = ['public_profile', 'email'];
    constructor(
        private platform: Platform,
        private fb: Facebook) {
        this.platform = platform;
    }

    login() {
        let promise = new Promise((resolve, reject) => {
            if (this.platform.is('cordova')) {
                this.fb.login(this.premissions).then(
                    (success) => {
                        resolve(success);
                    }, (err) => {
                        reject(err);
                    });
            } else {
                reject('Please run me on a device');
            }
        });
        return promise;
    }

    getCurrentUserProfile() {
        let promise = new Promise((resolve, reject) => {
            this.fb.api('me?fields=email,name,picture.type(large)', null).then(
                (profileData) => {
                    resolve(profileData);
                }, (err) => {
                    reject(err);
                });
        });
        return promise;
    }

}
