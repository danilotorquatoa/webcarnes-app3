/**
* User model
*/

export class User {
  id : number;
  facebook_id : number;
  type : string;
  name : string;
  cpf : string;
  phone: string;
  email: string;
  gender: string;
  dob: string;
  photo: string;
  token?: string;
  
  constructor(theUser:any){
    this.id = theUser.attributes.id;
    this.facebook_id = theUser.attributes.facebook_id;
    this.type = theUser.attributes.type;
    this.name = theUser.attributes.name;
    this.cpf = theUser.attributes.cpf;
    this.phone = theUser.attributes.phone;
    this.email = theUser.attributes.email;
    this.gender = theUser.attributes.gender;
    this.dob = theUser.attributes.dob;
    this.photo = theUser.attributes.photo;
    this.token = theUser.attributes.token;
  }
}
