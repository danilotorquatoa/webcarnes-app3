import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicApp, IonicModule } from 'ionic-angular';
import { NativeStorage } from '@ionic-native/native-storage';
import { MyApp } from './app.component';
import { TextMaskModule } from 'angular2-text-mask';
import { BrowserModule } from '@angular/platform-browser';
import { Facebook } from '@ionic-native/facebook';

// Pages
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';
import { AccountActivatePage } from '../pages/account-activate/account-activate';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { NewPasswordPage } from '../pages/new-password/new-password';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { HowWorksPage } from '../pages/how-works/how-works';
import { AdvantagesPage } from '../pages/advantages/advantages';
import { ProductsPage } from '../pages/products/products';
import { ProductsSinglePage } from '../pages/products-single/products-single';
import { ProductsNotAddressPage } from '../pages/products-not-address/products-not-address';
import { ProductsSelectAddressPage } from '../pages/products-select-address/products-select-address';
import { ProductsStoresPage } from '../pages/products-stores/products-stores';
import { ProductsStoresListPage } from '../pages/products-stores-list/products-stores-list';
import { CheckoutPage } from '../pages/checkout/checkout';
import { PaymentPage } from '../pages/payment/payment';
import { DeliveryPage } from '../pages/delivery/delivery';
import { ConfirmationPage } from '../pages/confirmation/confirmation';
import { OrdersPage } from '../pages/orders/orders';
import { MyOrdersPage } from '../pages/my-orders/my-orders';

// Components
import { HeadersComponent } from '../components/headers/headers';
import { HasAccountComponent } from '../components/has-account/has-account';
import { EqualValidator } from '../components/equal-validator/equal-validator';

// Providers
import { UserProvider } from '../providers/user-provider';
import { FbProvider } from '../providers/fb-provider';

@NgModule({
	declarations: [
		MyApp,
		LoginPage,
		AccountPage,
		AccountActivatePage,
		ResetPasswordPage,
		NewPasswordPage,
		HasAccountComponent,
		HeadersComponent,
		HomePage,
		ProfilePage,
		HowWorksPage,
		AdvantagesPage,
		ProductsPage,
		ProductsSinglePage,
		ProductsNotAddressPage,
		ProductsSelectAddressPage,
		ProductsStoresPage,
		ProductsStoresListPage,
		CheckoutPage,
		PaymentPage,
		DeliveryPage,
		ConfirmationPage,
		OrdersPage,
		MyOrdersPage,
        EqualValidator
	],
	imports: [
        BrowserModule,
		IonicModule.forRoot(MyApp),
        ReactiveFormsModule,
        TextMaskModule,
	],
	bootstrap: [IonicApp],
	entryComponents: [
		MyApp,
		LoginPage,
		AccountPage,
		AccountActivatePage,
		ResetPasswordPage,
		NewPasswordPage,
		HasAccountComponent,
		HeadersComponent,
		HomePage,
		ProfilePage,
		HowWorksPage,
		AdvantagesPage,
		ProductsPage,
		ProductsSinglePage,
		ProductsNotAddressPage,
		ProductsSelectAddressPage,
		ProductsStoresPage,
		ProductsStoresListPage,
		CheckoutPage,
		PaymentPage,
		DeliveryPage,
		ConfirmationPage,
		OrdersPage,
		MyOrdersPage
	],
	providers: [
        NativeStorage,
        UserProvider,
        Facebook,
        FbProvider
    ]
})
export class AppModule {}
