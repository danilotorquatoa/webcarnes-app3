import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, Events } from 'ionic-angular';
import { Deeplinks } from '@ionic-native/deeplinks';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { NativeStorage } from '@ionic-native/native-storage';

// Pages
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';
import { AccountActivatePage } from '../pages/account-activate/account-activate';
import { ResetPasswordPage } from '../pages/reset-password/reset-password';
import { NewPasswordPage } from '../pages/new-password/new-password';
import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { HowWorksPage } from '../pages/how-works/how-works';
import { AdvantagesPage } from '../pages/advantages/advantages';
import { ProductsPage } from '../pages/products/products';
import { ProductsSinglePage } from '../pages/products-single/products-single';
import { ProductsNotAddressPage } from '../pages/products-not-address/products-not-address';
import { ProductsSelectAddressPage } from '../pages/products-select-address/products-select-address';
import { ProductsStoresPage } from '../pages/products-stores/products-stores';
import { ProductsStoresListPage } from '../pages/products-stores-list/products-stores-list';
import { CheckoutPage } from '../pages/checkout/checkout';
import { PaymentPage } from '../pages/payment/payment';
import { DeliveryPage } from '../pages/delivery/delivery';
import { ConfirmationPage } from '../pages/confirmation/confirmation';
import { OrdersPage } from '../pages/orders/orders';
import { MyOrdersPage } from '../pages/my-orders/my-orders';

// Model
import { User } from '../models/user';

// Provide
import { UserProvider } from '../providers/user-provider';
import { FbProvider } from '../providers/fb-provider';

// Components
import { HeadersComponent } from '../components/headers/headers';
import { HasAccountComponent } from '../components/has-account/has-account';

@Component({
	templateUrl: 'app.html'

})
export class MyApp {
	@ViewChild(Nav) nav: Nav;
    user: User;
	rootPage: any;


	menu: Array<{title: string, component: any, classIcon: string}>;
	menuBottom: Array<{title: string, component: any}>;

	constructor(
        public events: Events,
		public platform: Platform,
		private storage: NativeStorage,
		private userProvide: UserProvider,
        private fb: Facebook,
		private fbProvider : FbProvider,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private deeplinks: Deeplinks) {
		this.initializeApp();

		// used for an example of ngFor and navigation
		this.menu = [
			{ title: 'Início', component: HomePage, classIcon: 'icon__init' },
			{ title: 'Produtos', component: ProductsSelectAddressPage, classIcon: 'icon__beef' },
			{ title: 'Receitas', component: AccountPage, classIcon: 'icon__recipes' },
			{ title: 'Churrascômetro', component: AccountActivatePage, classIcon: 'icon__barbecue' },
			{ title: 'Churrasqueiros', component: ResetPasswordPage, classIcon: 'icon__bb-gril' },
			{ title: 'Meus pedidos', component: NewPasswordPage, classIcon: 'icon__requests' }
		];

		this.menuBottom = [
			{ title: 'Vantagens', component: AdvantagesPage },
			{ title: 'Como funciona', component: HowWorksPage },
			{ title: 'Perguntas fequentes', component: AccountActivatePage },
			{ title: 'Sobre o Web Carnes', component: ResetPasswordPage }
		];
	}

	initializeApp() {
        this.platform.ready().then(() => {
            this.userProvide.hasLoggedIn().then(
                (hasLoggedIn) => {
                    if (hasLoggedIn) {
                        this.rootPage = HomePage;
                    } else {
                        this.rootPage = LoginPage;
                    }

                    this.deeplinks.routeWithNavController(this.nav, {
                        '/reset/:token/:email' : NewPasswordPage
                    }).subscribe(
                        (match) => {
                            console.log('Successfully matched route', match);
                        },
                        (nomatch) => {
                            // nomatch.$link - the full link data
                            console.error('Got a deeplink that didn\'t match', nomatch);
                        }
                    );

                    this.statusBar.styleLightContent();
                    this.splashScreen.hide();
                });
        });
	}

	openPage(page) {
		// Reset the content nav to have just this page
		// we wouldn't want the back button to show in this scenario
		this.nav.setRoot(page.component);
	}

	// Avatar clicked to profile page
	goToProfile(){
		this.nav.push(ProfilePage);
	}

	directives: [HasAccountComponent, HeadersComponent]
}
