import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { CheckoutPage } from '../../pages/checkout/checkout';



/*
  Generated class for the Headers component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
	selector: 'headers',
	templateUrl: 'headers.html'
})
export class HeadersComponent {

	@Input() title: string;
	@Input() displayMenuBar: boolean = false;
	@Input() checkout: boolean = true;
	@Input() filter: boolean = true;
  @Input() backPage: any = CheckoutPage;

	constructor(public navCtrl: NavController) {}

	goToCheckout(){
		this.navCtrl.setRoot(CheckoutPage)
	}

    goBack(){
			if(this.navCtrl.canGoBack()){
        this.navCtrl.pop();
			}else{
				console.log("não volta já mais");
			}
    }

}
