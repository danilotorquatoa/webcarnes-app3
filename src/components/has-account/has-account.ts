import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

// Pages
import { LoginPage } from '../../pages/login/login';
import { AccountPage } from '../../pages/account/account';

/*
	Generated class for the HasAccount component.

	See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
	for more info on Angular 2 Components.
*/
@Component({
	selector: 'has-account',
	templateUrl: 'has-account.html',
})
export class HasAccountComponent {

	@Input() hasAccount: boolean = false;

	constructor(public navCtrl: NavController) {}

	redirectLogin() {
		this.navCtrl.setRoot(LoginPage);
	}

	redirectPage() {
		this.navCtrl.push(AccountPage);
	}

}
