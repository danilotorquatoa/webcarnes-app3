import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGE
import { PaymentPage } from '../payment/payment';



/*
  Generated class for the Delivery page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-delivery',
	templateUrl: 'delivery.html'
})
export class DeliveryPage {

	selected: boolean = false;
	delivery: Array<{
		id: number,
		type: string,
		time: string,
		price: string,
		status: boolean
	}>;

	constructor(public navCtrl: NavController) {
		this.delivery = [
			{id: 0, type: 'Normal', time: '30min', price: 'R$ 12,90', status: false},
			{id: 1, type: 'Expressa', time: '10min', price: 'R$ 22,90', status: false}
		];
	}

	buttonState(id) {
		this.delivery.forEach(function(selected) {
			if (selected.id == id) {
				selected.status = !selected.status;
				console.log(selected.status);
			}
		});
	}

	deliveryScheduled(){
		this.selected = !this.selected;
	}

	goToPayment(){
		this.navCtrl.setRoot(PaymentPage);
	}

}
