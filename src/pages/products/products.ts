import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { ProductsSinglePage } from '../products-single/products-single';

/*
  Generated class for the Products page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-products',
	templateUrl: 'products.html'
})
export class ProductsPage {
	products : string;
	tabs: Array<{
		tabName: string,
		productsTabName: Array<{nameProduct: string, priceProduct: string, brandProduct: string, stamp: boolean}>
	}>;

	constructor(public navCtrl: NavController) {
		this.products = '0';
		this.tabs = [
			{
				tabName: 'Bovinos',
				productsTabName: [
					{nameProduct: 'AALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'Ancho Docs', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'AALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'ALCATRA', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false}
				]
			},
			{
				tabName: 'Suinos',
				productsTabName: [
					{nameProduct: 'ALCATRA MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'AALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'ALCATRA', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false}
				]
			},
			{
				tabName: 'Aves',
				productsTabName: [
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'ALCATRA MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'AALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'ALCATRA', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false}
				]
			},
			{
				tabName: 'Miúdos',
				productsTabName: [
					{nameProduct: 'ALCATRA MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'AALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'Ancho Doc', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false},
					{nameProduct: 'ALCATRA MIOLO MASTER GRILL', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: true},
					{nameProduct: 'ALCATRA', priceProduct: 'R$ 22,45 / KG', brandProduct: 'Sadia', stamp: false}
				]
			}
		];
	}

	goToSinlge(){
		this.navCtrl.setRoot(ProductsSinglePage);
	}
}
