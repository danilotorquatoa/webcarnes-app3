import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { ProductsPage } from '../products/products';
import { CheckoutPage } from '../checkout/checkout';

/*
  Generated class for the ProductsSingle page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-products-single',
	templateUrl: 'products-single.html'
})
export class ProductsSinglePage {

	typeCut: string = "corte 1";
	relatedProducts: Array<{
		imageProduct: string,
		nameProduct: string,
		priceProduct: string,
		brandProduct: string,
		stamp: boolean
	}>;
	typeCutTitle: {subTitle: string };
	purchaseBtn: boolean = true;

	constructor(public navCtrl: NavController) {

		this.relatedProducts = [
			{imageProduct: 'assets/images/thumb-related.jpg', nameProduct: 'Molho B.B.Q', priceProduct: 'R$ 8,90', brandProduct: 'Heinz', stamp: true},
			{imageProduct: 'assets/images/thumb-related-1.jpg', nameProduct: 'Molho B.B.Q', priceProduct: 'R$ 8,90', brandProduct: 'Tondela', stamp: false},
		];

		this.typeCutTitle = {
			subTitle: 'Tipos de cortes'
		};

	}


	purchase(){
		this.purchaseBtn = !this.purchaseBtn;
	}

	goTo(){
		this.navCtrl.setRoot(ProductsPage);
	}

	goToCheckout(){
		this.navCtrl.setRoot(CheckoutPage)
	}
}
