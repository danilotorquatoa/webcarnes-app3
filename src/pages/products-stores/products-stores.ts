import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProductsStoresListPage } from '../products-stores-list/products-stores-list';

/*
  Generated class for the ProductsStores page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-products-stores',
	templateUrl: 'products-stores.html'
})
export class ProductsStoresPage {

	stores: Array<{
		brand: string,
		title: string,
		distance: string
	}>;

	constructor(public navCtrl: NavController) {

		this.stores = [
			{brand: 'assets/images/stores/store-1.jpg', title: 'Carnes Gourmet', distance: '0.5km'},
			{brand: 'assets/images/stores/store-2.jpg', title: 'Empório da carne', distance: '0.5km'},
			{brand: 'assets/images/stores/store-3.jpg', title: 'espaço da carne', distance: '1.5km'},
			{brand: 'assets/images/stores/store-1.jpg', title: 'Carnes Gourmet', distance: '0.5km'},
			{brand: 'assets/images/stores/store-2.jpg', title: 'Empório da carne', distance: '0.5km'},
			{brand: 'assets/images/stores/store-3.jpg', title: 'espaço da carne', distance: '1.5km'},
			{brand: 'assets/images/stores/store-1.jpg', title: 'Carnes Gourmet', distance: '0.5km'},
			{brand: 'assets/images/stores/store-2.jpg', title: 'Empório da carne', distance: '0.5km'},
			{brand: 'assets/images/stores/store-3.jpg', title: 'espaço da carne', distance: '1.5km'},
			{brand: 'assets/images/stores/store-1.jpg', title: 'Carnes Gourmet', distance: '0.5km'},
			{brand: 'assets/images/stores/store-2.jpg', title: 'Empório da carne', distance: '0.5km'},
			{brand: 'assets/images/stores/store-3.jpg', title: 'espaço da carne', distance: '1.5km'}
		];
	}
	goTo(){
		this.navCtrl.setRoot(ProductsStoresListPage);
	}

}
