import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


// PAGE
import { MyOrdersPage } from '../my-orders/my-orders';
/*
  Generated class for the Orders page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-orders',
	templateUrl: 'orders.html'
})
export class OrdersPage {

	constructor(public navCtrl: NavController) {}

	goToOrder(){
		this.navCtrl.setRoot(MyOrdersPage);
	}

}
