import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { LoginPage } from '../login/login';
import { HomePage } from '../home/home';

import { UserProvider } from '../../providers/user-provider';

/*
Generated class for the AccountActivate page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
	selector: 'page-account-activate',
	templateUrl: 'account-activate.html'
})
export class AccountActivatePage {
	public activateForm: FormGroup;

	constructor(
		public navCtrl: NavController,
		private fb: FormBuilder,
		public modalCtrl: AlertController,
		private userProvider: UserProvider,
		public loadingCtrl: LoadingController) {}

	ngOnInit(){
        this.activateForm = this.fb.group({
            code : ["", [Validators.required, Validators.minLength(6)]]
        });
    }

    activate(code: string, isValid: boolean){
    	let loadingPopup = this.loadingCtrl.create({
            content: 'Aguarde...'
        });

        this.presenterLoading(loadingPopup)

        if(isValid){
        	this.userProvider.activate(code).subscribe(
        		(res) => {
        			this.presenterLoading(loadingPopup, false);
                    this.accountActiveted(res);
        		},
        		(err) => {
                    this.presenterLoading(loadingPopup, false);
                    if(err.errors) {
                        let msg = '';
                        let errors = Object.keys(err.errors);
                        let form = this.activateForm;

                        errors.forEach(function (element, index) {
                            msg += err.errors[element][0] + '<br/>';
                            form.controls[element].setErrors({
                                "notUnique": true
                            });
                        });

                        this.accountActiveError(msg);
                    }else{
                        this.accountActiveError(err.message);
                    }
                }
        	);
        }else{
        	this.accountActiveError('O campo código é obrigatório.');
        }
    }

    accountActiveted(message) {
		let createModal = this.modalCtrl.create({
			title: 'Conta ativada.',
			message: message,
			cssClass: 'modal__default modal__success',  // modal__success, modal__error, modal__warning
			buttons: ['OK']
		});

        createModal.onDidDismiss(data => {
            this.activateForm.reset();
            this.navCtrl.setRoot(HomePage);
        });

		createModal.present();
	}

    accountActiveError(message) {
		let errorModal = this.modalCtrl.create({
			title: 'Erro ao tentar ativar a conta.',
			message: message,
			cssClass: 'modal__default modal__error',  // modal__success, modal__error, modal__warning
			buttons: ['OK']
		});

		errorModal.present();
	}

	presenterLoading(popup, show: boolean = true){
        if(show){
            popup.present();
        }else{
            popup.dismiss();
        }
    }

}
