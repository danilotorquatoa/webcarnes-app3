import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

// Pages
import { LoginPage } from '../../pages/login/login';

// Providers
import { UserProvider } from '../../providers/user-provider';
import { CustomValidators } from '../../providers/custom-validators';

/*
  Generated class for the ResetPassword page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
    selector: 'page-reset-password',
    templateUrl: 'reset-password.html'
})
export class ResetPasswordPage {
    public recoverForm: FormGroup;

    constructor(
        public navCtrl: NavController,
        private fb: FormBuilder,
        public modalCtrl: AlertController,
        private userProvider: UserProvider,
        public loadingCtrl: LoadingController) {}

  	ngOnInit(){
        this.recoverForm = this.fb.group({
            email: ["", [Validators.required, CustomValidators.email]]
        });
    }

    recover(email: string, isValid: boolean){
    	let loadingPopup = this.loadingCtrl.create({
            content: 'Aguarde...'
        });

        this.presenterLoading(loadingPopup);

        if(isValid){
            this.userProvider.recover(email).subscribe(
        		(res) => {
        			this.presenterLoading(loadingPopup, false);
                    this.recoverSuccess(res);
        		},
        		(err) => {
                    this.presenterLoading(loadingPopup, false);
                    this.recoverError(err);
                }
        	);
        }else{
        	this.recoverError('O campo email é obrigatório.');
        }
    }

    recoverSuccess(message) {
		let createModal = this.modalCtrl.create({
			title: 'Recurperar Senha.',
			message: message,
			cssClass: 'modal__default modal__success',  // modal__success, modal__error, modal__warning
			buttons: ['OK']
		});

        createModal.onDidDismiss(data => {
            this.recoverForm.reset();
            this.navCtrl.setRoot(LoginPage);
        });

		createModal.present();
	}

    recoverError(message) {
        let errorModal = this.modalCtrl.create({
            title: 'Erro ao tentar recuperar a senha.',
            message: message,
            cssClass: 'modal__default modal__error',  // modal__success, modal__error, modal__warning
            buttons: ['OK']
        });

        errorModal.present();
    }

    presenterLoading(popup, show: boolean = true){
        if(show){
            popup.present();
        }else{
            popup.dismiss();
        }
    }

  }
