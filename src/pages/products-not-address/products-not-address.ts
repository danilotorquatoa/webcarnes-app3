import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the ProductsNotAddress page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-products-not-address',
  templateUrl: 'products-not-address.html'
})
export class ProductsNotAddressPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello ProductsNotAddressPage Page');
  }

}
