import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { ProductsSinglePage } from '../products-single/products-single';
import { DeliveryPage } from '../delivery/delivery';




/*
  Generated class for the Checkout page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-checkout',
	templateUrl: 'checkout.html'
})
export class CheckoutPage {

	relatedProducts: Array<{
		imageProduct: string,
		nameProduct: string,
		priceProduct: string,
		brandProduct: string,
		stamp: boolean
	}>;
	keyupValue: string = '';


	constructor(public navCtrl: NavController) {

		this.relatedProducts = [
			{imageProduct: 'assets/images/thumb-related.jpg', nameProduct: 'Molho B.B.Q', priceProduct: 'R$ 8,90', brandProduct: 'Heinz', stamp: true},
			{imageProduct: 'assets/images/thumb-related-1.jpg', nameProduct: 'Molho B.B.Q', priceProduct: 'R$ 8,90', brandProduct: 'Tondela', stamp: false},
		];
	}

	validCupom(cupom){
		this.keyupValue = cupom === 'index' ? 'correct' : 'wrong';
	}

	goToSinlge(){
		this.navCtrl.setRoot(ProductsSinglePage);
	}

	goToPayment(){
		this.navCtrl.setRoot(DeliveryPage);
	}
}
