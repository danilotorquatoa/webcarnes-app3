import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';

// Page
import { HomePage } from '../home/home';

/*
  Generated class for the HowWorks page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
  */
@Component({
  	selector: 'page-how-works',
  	templateUrl: 'how-works.html'
})
export class HowWorksPage {

	@ViewChild('howWorksSlider') slider: Slides;

	constructor(public navCtrl: NavController) {}


	goToNext() {
		this.slider.slideNext();
	}

	goToPrev() {
		this.slider.slidePrev();
	}

	goToHome(){
		this.navCtrl.setRoot(HomePage);
	}

}
