import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, NavParams,ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Facebook } from '@ionic-native/facebook';

import { Camera, CameraOptions } from '@ionic-native/camera';

//import { LoginPage } from '../login/login';
import { AccountActivatePage } from '../account-activate/account-activate';
import { HomePage } from '../home/home';


import { User } from '../../models/user';
import { UserProvider } from '../../providers/user-provider';
import { CustomValidators } from '../../providers/custom-validators';

/*
Generated class for the Account page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
	selector: 'page-account',
	templateUrl: 'account.html',
})
export class AccountPage {
	public registerForm: FormGroup; // our model driven form
	public phoneMask: Array<string | RegExp>;
	public cpfMask: Array<string | RegExp>;
	public base64Image: string;
	public isEdite: boolean;
	public fbAccount: any;
	public cameraOptions: CameraOptions;

	//    backPage: any = LoginPage;
	user: User;
	hasPicture = false;

	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public modalCtrl: AlertController,
		private userProvider: UserProvider,
		private storage: Storage,
		private fb: FormBuilder,
		public toastCtrl: ToastController,
		public loadingCtrl: LoadingController,
		private facebook: Facebook,
		private camera: Camera) {

			this.phoneMask = ['(', /[0-9]/, /\d/, ')', /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/, /\d/];
			this.cpfMask = [/[0-9]/, /\d/, /\d/,'.', /\d/, /\d/, /\d/, '.', /\d/, /\d/, /\d/, '-', /\d/, /\d/];

			this.isEdite = this.navParams.get("isEdite");
			this.fbAccount = this.navParams.get("fbAccount");

			this.cameraOptions = {
				destinationType: this.camera.DestinationType.DATA_URL,
				targetWidth: 500,
				targetHeight: 500,
				cameraDirection: this.camera.Direction.FRONT,
				saveToPhotoAlbum: false,
				quality: 100,
			}

		}




		save(model: User, isValid: boolean) {
			let loadingPopup = this.loadingCtrl.create({
				content: 'Aguarde...'
			});

			this.presenterLoading(loadingPopup)

			if(isValid){
				delete model['email_confirmation'];

				model['cpf'] = model['cpf'].replace(/[^A-Za-z0-9]/g, '');
				model['phone'] = model['phone'].replace(/[^A-Za-z0-9]/g, '');

				this.userProvider.register(model).subscribe(
					(res) => {
						this.presenterLoading(loadingPopup, false);
						if(model['facebook_id']){
							this.accountCreate('Conta cria com sucesso.');
						}else{
							this.accountCreate('Verifique seu email e use o código para ativar sua conta');
						}
					},
					(err) => {
						this.presenterLoading(loadingPopup, false);
						if(err.errors) {
							let msg = '';
							let errors = Object.keys(err.errors);
							let form = this.registerForm;

							errors.forEach(function (element, index) {
								msg += err.errors[element][0] + '<br/>';
								form.controls[element].setErrors({
									"notUnique": true
								});
							});

							this.accountError(msg);
						}else{
							this.accountError(err.message);
						}
					}
				);
			}else{
				this.accountError('Preencha todos os campos.');
			}
		}

		presenterLoading(popup, show: boolean = true){
			if(show){
				popup.present();
			}else{
				popup.dismiss();
			}
		}

		ngOnInit(){
			this.registerForm = this.fb.group({
				facebook_id: [""],
				photo: [""],
				name : ["", [Validators.required, Validators.minLength(5)]],
				phone: ["", [Validators.required]],
				cpf: ["", [Validators.required]],
				email: ["", [Validators.required, CustomValidators.email]],
				email_confirmation: ["", [Validators.required, CustomValidators.email]],
				password: ["", [Validators.required, Validators.minLength(8)]],
				password_confirmation: ["", [Validators.required, Validators.minLength(8)]]
			});

			if(this.isEdite){
				this.storage.get('profile').then(
					(res) => {
						this.user = res;
						if(res){
							this.base64Image = 'http://api.webcarnes.idx.digital/'+this.user.photo;
							this.registerForm.controls['photo'].setValue(this.user.photo);
							this.registerForm.controls['name'].setValue(this.user.name);
							this.registerForm.controls['phone'].setValue(this.user.phone);
							this.registerForm.controls['cpf'].setValue(this.user.cpf);
							this.registerForm.controls['email'].setValue(this.user.email);
						}

					},
					(err) => {
						console.log(err);
					}
				);
			}else if(this.fbAccount){
						this.base64Image = this.fbAccount.picture.data.url;
						this.registerForm.controls['photo'].setValue(this.fbAccount.picture.data.url);
						this.registerForm.controls['name'].setValue(this.fbAccount.name);
						this.registerForm.controls['email'].setValue(this.fbAccount.email);
						this.registerForm.controls['email_confirmation'].setValue(this.fbAccount.email);
						this.registerForm.controls['facebook_id'].setValue(this.fbAccount.id);
			}
		}

		// TAKE A PICTURE
		takePicture(){
			this.camera.getPicture(this.cameraOptions).then((imageData) => {
				// imageData is a base64 encoded string
				if(imageData) {
					this.hasPicture = true;
					this.base64Image = "data:image/jpeg;base64," + imageData;
					this.registerForm.controls['photo'].setValue(this.base64Image)
				}
			}, (err) => {
				console.log(err);
			});
		}

		FBlogin(){
			let loadingPopup = this.loadingCtrl.create({
				content: 'Aguarde...'
			});

			this.facebook.getAccessToken()
			.then(
				(res) => {
					this.userProvider.loginFBAccount(res)
					.subscribe(
						(user) => {
							this.presenterLoading(loadingPopup, false);
							this.storage.set('profile', user );
							this.navCtrl.setRoot(HomePage);
						},
						(err) => {
							this.accountError(err);
						}
					)
				}
			);
		}

		accountCreate(message) {
			let createModal = this.modalCtrl.create({
				title: 'Cadastro Realizado com sucesso',
				message: message,
				cssClass: 'modal__default modal__success',  // modal__success, modal__error, modal__warning
				buttons: ['OK']
			});

			createModal.onDidDismiss(data => {
				if(this.registerForm.controls['facebook_id'].value){
					this.FBlogin();
				}else{
					this.registerForm.reset();
					this.navCtrl.setRoot(AccountActivatePage);
				}
			});

			createModal.present();
		}


		accountError(message) {
			let errorModal = this.modalCtrl.create({
				title: 'Erro ao tentar realizar o cadastro.',
				message: message,
				cssClass: 'modal__default modal__error',  // modal__success, modal__error, modal__warning
				buttons: ['OK']
			});

			errorModal.present();
		}


	}
