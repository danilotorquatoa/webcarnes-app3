import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


// PAGES
import { HomePage } from '../home/home';
import { OrdersPage } from '../orders/orders';

/*
  Generated class for the Confirmation page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-confirmation',
	templateUrl: 'confirmation.html'
})
export class ConfirmationPage {

	loading: boolean = true;

	constructor(public navCtrl: NavController) {
		setTimeout(() => this.loading = false, 4000);
	}

	goToOrders(){
		this.navCtrl.setRoot(OrdersPage);
	}

	goToHome(){
		this.navCtrl.setRoot(HomePage);
	}

}
