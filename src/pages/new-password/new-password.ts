import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

import { UserProvider } from '../../providers/user-provider';
import { LoginPage } from '../../pages/login/login';

/*
Generated class for the NewPassword page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
  selector: 'page-new-password',
  templateUrl: 'new-password.html'
})
export class NewPasswordPage {
  public resetForm: FormGroup;
  token: string;
  email: string;

  constructor(public navCtrl: NavController,
    private _params: NavParams,
    private fb: FormBuilder,
    public modalCtrl: AlertController,
    private userProvider: UserProvider,
    public loadingCtrl: LoadingController) {
      this.token = decodeURIComponent(_params.data.token);
      this.email = decodeURIComponent(_params.data.email);

      console.log(this.token);
      console.log(this.email);
    }

    ngOnInit(){
      this.resetForm = this.fb.group({
        email: [this.email, [Validators.required]],
        token: [this.token, [Validators.required]],
        password: ["", [Validators.required, Validators.minLength(8)]],
        password_confirmation: ["", [Validators.required, Validators.minLength(8)]]
      });
    }

    reset(form: any, isValid: boolean){
      let loadingPopup = this.loadingCtrl.create({
        content: 'Aguarde...'
      });

      this.presenterLoading(loadingPopup);

      if(isValid){
        this.userProvider.reset(form).subscribe(
          (res) => {
            this.presenterLoading(loadingPopup, false);
            this.resetSuccess(res);
          },
          (err) => {
            this.presenterLoading(loadingPopup, false);
            this.resetError(err);
          }
        );
      }else{
        this.resetError('Preencha todos os campos.');
      }
    }

    resetSuccess(message) {
      let createModal = this.modalCtrl.create({
        title: 'Nova Senha.',
        message: message,
        cssClass: 'modal__default modal__success',  // modal__success, modal__error, modal__warning
        buttons: ['OK']
      });

      createModal.onDidDismiss(data => {
        this.resetForm.reset();
        this.navCtrl.setRoot(LoginPage);
      });

      createModal.present();
    }

    resetError(message) {
      let errorModal = this.modalCtrl.create({
        title: 'Erro ao tentar criar  uma nova senha.',
        message: message,
        cssClass: 'modal__default modal__error',  // modal__success, modal__error, modal__warning
        buttons: ['OK']
      });

      errorModal.present();
    }

    presenterLoading(popup, show: boolean = true){
      if(show){
        popup.present();
      }else{
        popup.dismiss();
      }
    }
  }
