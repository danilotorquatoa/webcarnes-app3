import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { CheckoutPage } from '../checkout/checkout';

/*
  Generated class for the MyOrders page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-my-orders',
	templateUrl: 'my-orders.html'
})
export class MyOrdersPage {

	constructor(public navCtrl: NavController) {}


	goToCheckout(){
		this.navCtrl.setRoot(CheckoutPage);
	}

}
