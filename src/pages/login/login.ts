import { Component } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';

import { AccountPage } from '../account/account';
import { HomePage } from '../home/home';
import { ResetPasswordPage } from '../reset-password/reset-password';

import { User } from '../../models/user';
import { UserProvider } from '../../providers/user-provider';
import { FbProvider } from '../../providers/fb-provider';

/*
Generated class for the Login page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
	selector: 'page-login',
	templateUrl: 'login.html'
})

export class LoginPage {
	user: User;
	public loginForm = this.fb.group({
		email: ["", [Validators.required, Validators.pattern("^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$")]],
		password: ["", [Validators.required, Validators.minLength(6)]]
	});

	constructor(
		public navCtrl: NavController,
		private userProvider: UserProvider,
		private storage: Storage,
		public fb: FormBuilder,
		public toastCtrl: ToastController,
		public loadingCtrl: LoadingController,
		public facebook: FbProvider) {}

		login() {
			let loadingPopup = this.loadingCtrl.create({
				content: 'Aguarde...'
			});
			this.presenterLoading(loadingPopup);
			if(this.loginForm.valid) {
				this.userProvider.login(this.loginForm.value.email, this.loginForm.value.password)
				.subscribe(
					user => {
						this.presenterLoading(loadingPopup, false)
						this.user = user;
						this.storage.set('profile', this.user );
						this.navCtrl.setRoot(HomePage);
						this.loginForm.reset();
					},
					err => {
						this.loginForm.reset();
						this.presenterLoading(loadingPopup, false)
						this.presenterToast(err);
					}
				);
			}
		}

		fbLogin(){
			let loadingPopup = this.loadingCtrl.create({
				content: 'Aguarde...'
			});

			this.facebook.login().then(
				(res:FacebookLoginResponse) =>{
					this.presenterLoading(loadingPopup);
					this.userProvider.loginFBAccount(res.authResponse.accessToken)
					.subscribe(
						(user) => {
							this.presenterLoading(loadingPopup, false);
							this.user = user;
							this.storage.set('profile', this.user );
							this.navCtrl.setRoot(HomePage);
						},
						(err) => {
							let fbAuthResponse = res.authResponse;
							this.facebook.getCurrentUserProfile().then(
								(res:any) => {
									this.presenterLoading(loadingPopup, false);
									//let fbAccount = res;
									//this.storage.set('fb-profile', fbAccount);
									this.navCtrl.push(AccountPage, {fbAccount: res})
								},
								(err:any) => {
									this.presenterLoading(loadingPopup, false);
									this.presenterToast(err);
								}
							);
						}
					)
				},
				(err:FacebookLoginResponse) => {
					this.presenterLoading(loadingPopup, false);
					this.presenterToast(err);
				}
			);
		}

		presenterToast(err){
			let toast = this.toastCtrl.create({
				message: err,
				duration: 3000
			});
			toast.present();
		}

		presenterLoading(popup, show: boolean = true){
			if(show){
				popup.present();
			}else{
				popup.dismiss();
			}
		}

		redirectPage() {
			this.navCtrl.push(AccountPage);
		}

		resetPassword() {
			this.navCtrl.push(ResetPasswordPage);
		}
	}
