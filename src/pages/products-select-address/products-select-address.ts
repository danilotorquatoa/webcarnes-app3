import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


// Pages
import { ProductsStoresPage } from '../products-stores/products-stores';

/*
  Generated class for the ProductsSelectAddress page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-products-select-address',
	templateUrl: 'products-select-address.html'
})
export class ProductsSelectAddressPage {

	check: boolean = false;
	allAddress: Array<{
		id: number,
		typeAddress: string,
		address: string,
		postCodeCity: string,
		status: boolean
	}>;

	constructor(public navCtrl: NavController) {

		this.allAddress = [
			{id: 0, typeAddress: 'Casa', address: 'Rua das Andorinhas, 326', postCodeCity: '60123-000 - Fortaleza-CE', status: false},
			{id: 1, typeAddress: 'Trabalho', address: 'Rua das Andorinhas, 326', postCodeCity: '60123-000 - Fortaleza-CE', status: false},
			{id: 2, typeAddress: 'Casa da minha mãe', address: 'Rua das Andorinhas, 326', postCodeCity: '60123-000 - Fortaleza-CE', status: false},
			{id: 3, typeAddress: 'Casa da minha Sogra', address: 'Rua das Andorinhas, 326', postCodeCity: '60123-000 - Fortaleza-CE', status: false},
			{id: 4, typeAddress: 'Trabalho esposa', address: 'Rua das Andorinhas, 326', postCodeCity: '60123-000 - Fortaleza-CE', status: false}
		];
	}

	buttonState(id) {
		this.allAddress.forEach(function(checkbox) {
			if (checkbox.id !== id) {
				checkbox.status = !checkbox.status;
			}
		});
		this.check = !this.check;
	}

	goToProductsStoresPage(){
		this.navCtrl.setRoot(ProductsStoresPage);
	}

}
