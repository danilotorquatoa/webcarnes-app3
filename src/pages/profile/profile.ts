import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { User } from '../../models/user';

// Page
import { LoginPage } from '../login/login';
import { AccountPage } from '../account/account';
import { NewPasswordPage } from '../new-password/new-password';
/*
Generated class for the Profile page.

See http://ionicframework.com/docs/v2/components/#navigation for more info on
Ionic pages and navigation.
*/
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html'
})
export class ProfilePage {
  user: User;
  constructor(public navCtrl: NavController,		private storage: Storage,) {}

  ngOnInit(){
    this.storage.get('profile').then(
      (res) => {
        this.user = res;
      },
      (err) => {
        console.log(err);
      }
    );

  }


  logout(){
    this.storage.clear();
    this.navCtrl.setRoot(LoginPage)
  }

  goToEditeProfile(){
    this.navCtrl.push(AccountPage, {isEdite: true})
  }

  goToNewPassword(){
    this.navCtrl.push(NewPasswordPage,{token: this.user.token,email:this.user.email});
  }

}
