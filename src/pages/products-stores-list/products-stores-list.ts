import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


// PAGE
import { ProductsPage } from '../products/products';

/*
  Generated class for the ProductsStoresList page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-products-stores-list',
	templateUrl: 'products-stores-list.html'
})
export class ProductsStoresListPage {

	categoryProductsStores: Array<{
		name: string,
		thumb: string
	}>;

	constructor(public navCtrl: NavController) {

		this.categoryProductsStores = [
			{name: 'Carnes', thumb: 'assets/images/stores/list-1.jpg'},
			{name: 'Temperos', thumb: 'assets/images/stores/list-2.jpg'},
			{name: 'Cervejas', thumb: 'assets/images/stores/list-3.jpg'},
			{name: 'Molhos', thumb: 'assets/images/stores/list-4.jpg'},
			{name: 'Carnes', thumb: 'assets/images/stores/list-1.jpg'},
			{name: 'Temperos', thumb: 'assets/images/stores/list-2.jpg'},
			{name: 'Cervejas', thumb: 'assets/images/stores/list-3.jpg'},
			{name: 'Molhos', thumb: 'assets/images/stores/list-4.jpg'}
		];
	}


	goTo(){
		this.navCtrl.setRoot(ProductsPage);
	}



}
