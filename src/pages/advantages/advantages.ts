import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';

// Page
import { HomePage } from '../home/home';
/*
  Generated class for the Advantages page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-advantages',
	templateUrl: 'advantages.html'
})
export class AdvantagesPage {

	@ViewChild('advantagesSlider') slider: Slides;

	constructor(public navCtrl: NavController) {}

	advantagesSliderOptions = {
		pager: true
	};

	goToHome(){
		this.navCtrl.setRoot(HomePage);
	}

}
