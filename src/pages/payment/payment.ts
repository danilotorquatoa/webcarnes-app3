import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// PAGES
import { ConfirmationPage } from '../confirmation/confirmation';


/*
  Generated class for the Payment page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
	selector: 'page-payment',
	templateUrl: 'payment.html'
})
export class PaymentPage {

	exchange: boolean = false;
	hasExchange: boolean = false;
	hasExchanges: boolean = false;
	hasCards: boolean = false;

	constructor(public navCtrl: NavController) {}


	delivery(){
		this.exchange = !this.exchange;
	}

	getExchange(){
		this.hasExchange = !this.hasExchange;
	}

	getExchanges(){
		this.hasExchanges = !this.hasExchanges;
	}

	flagCards(){
		this.hasCards = !this.hasCards;

	}

	confirmation(){
		this.navCtrl.setRoot(ConfirmationPage);
	}

}
